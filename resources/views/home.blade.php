@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Welcome to this beautiful admin panel.</p>
    @if(Session::has('success_message'))
        <div class="alert alert-success" id="successMessage">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
        </div>
    @endif
    <div>
        <button type="button" class="btn btn-success" style="float:right;" id="addEmployee">Add New</button>
    </div>

    <!-- Modal for Add new employee --> 
    <div class="modal fade" id="addEmployeePop"  tabindex="-1" role="dialog" aria-labelledby="addModeratorLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Employee</h5>
            </div>
            <form name="employeeForm" method="POST" id="employeeForm" action="{{ route('saveEmployee') }}" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-body text-start add-moderator-form p-sm-5 pb-sm-0">
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input class="form-control" type="text" name="name" id="name" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input class="form-control" type="email" name="email" id="email" onblur="checkEmail()" required>
                        <span id="mail_exist_err" style="color:red;display:none;">Email already exists.</span>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Designation</label>
                        <div class="position-relative">
                            <select class="form-control" name="designation" id="designation" required>
                                <option value="">- Select -</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Profile Pic</label>
                        <div class="position-relative">
                            <input class="form-control" type="file" name="profile_pic" id="profile_pic">
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer pe-sm-5 pb-4">
                <button type="button" class="btn btn-secondary employeeClose">Cancel</button>
                <button type="submit" class="btn btn-primary" id="saveEmployee">Submit</button>
            </div>
            </div>
        </div>
    </div>

    <!-- Modal for edit employee --> 
    <div class="modal fade" id="editEmployeePop"  tabindex="-1" role="dialog" aria-labelledby="addModeratorLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Employee</h5>
            </div>
            <form name="editEmployeeForm" method="POST" id="editEmployeeForm" action="{{ route('updateEmployee') }}" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="modal-body text-start add-moderator-form p-sm-5 pb-sm-0">
                    <input type="hidden" name="user_id" id="user_id">
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input class="form-control" type="text" name="edit_name" id="edit_name" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input class="form-control" type="email" name="edit_email" id="edit_email" readonly>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Designation</label>
                        <div class="position-relative">
                            <select class="form-control" name="edit_designation" id="edit_designation" required>
                                <option value="">- Select -</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer pe-sm-5 pb-4">
                <button type="button" class="btn btn-secondary editemployeeClose">Cancel</button>
                <button type="submit" class="btn btn-primary" id="updateEmployee">Submit</button>
            </div>
            </div>
        </div>
    </div>

    <!-- Modal Content Code for Delete Election-->
    <div class="modal fade" tabindex="-1" id="deleteModal">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable custom-modal" role="document">
            <div class="modal-content">
                <a href="javascript:void(0)" class="close deleteCancel" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
                <div class="modal-header justify-content-center">
                    <h5 class="modal-title">Confirmation</h5>
                </div>
                <div class="modal-body py-5">
                    <form method="POST" name="deletionForm" id="deletionForm">
                        @csrf
                        <input type="hidden" name="emp_id" id="emp_id" value=""/>
                        <p class="text-center">Are you sure you want to delete the employee ?</p>
                        <div class="modal-footer justify-content-center">
                            <button type="button" id="saveDeleteEmployee" class="btn btn-success px-4">Yes</button>
                            <button type="button" class="btn btn-danger deleteCancel px-4" data-dismiss="modal">No</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <table class="yajra-datatable table align-middle table-borderless table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Designation</th>
                <th>Action</th>
            </tr>
        </thead>
            <tbody></tbody> 
    </table>
@stop

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
    <style type="text/css">
        .error {
            color : red;
        }
    </style>
@stop

@section('js')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>

<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>


<script type="text/javascript">
var ytable;
$(function () {  
    ytable = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        order: [],
        ajax: {
            url: "{{ route('home') }}",
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'designation', name: 'designation'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });

  
$("body").on('click','#editEmp', function (e){
    var id = $(this).attr('data-id');
    $.ajax({
            url: "/getEmployee/"+id,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                console.log(data.result);
                if (data.message == 'success') {
                    $('#user_id').val(data.result.user_id);
                    $('#edit_name').val(data.result.name);
                    $('#edit_email').val(data.result.email);
                    $('#edit_designation').val(data.result.role_id);
                    $('#editEmployeePop').modal('show');
                } else {
                    $('#editEmployeePop').modal('hide');
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
}); 

$("body").on('click','#addEmployee', function (e){
    $('#addEmployeePop').modal('show');
}); 

$("#editEmployeeForm").validate({
    rules: {
        edit_name: "required",
        edit_email: {
            required: true,
            email: true,
            accept:"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}"
        },
        edit_designation: "required",
    }
});

$('#updateEmployee').on('click',function (e) {
    var validate = $("#editEmployeeForm").valid();
    if(validate) {
        $('#editEmployeeForm').submit();
    }
});

$(".employeeClose").on('click', function (e) {
    $('#employeeForm').trigger("reset");
    $('input').removeClass('error');
    $('.error').hide();
    $('#addEmployeePop').modal('hide');
});

$("#employeeForm").validate({
    rules: {
        name: "required",
        email: {
            required: true,
            email: true,
            accept:"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}"
        },
        designation: "required",
    }
});

/** Add new employee */
$('#saveEmployee').on('click',function (e) {
    var validate = $("#employeeForm").valid();
    if(validate) {
        $('#employeeForm').submit();
    }
});

/** Open the model pop up */
$("body").on('click','#deleteEmp', function (e){
    var empId = $(this).attr('data-id');
    $("#emp_id").val(empId);
    $('#deleteModal').modal('show');
});

/** Close the model pop up */
$(".deleteCancel").on('click', function (e) {
    $('#deletionForm').trigger("reset");
    $('#deleteModal').modal('hide');
});

/** Delete employee */
$('#saveDeleteEmployee').on('click',function (e) {
    e.preventDefault();
    $.ajax({
            data: $('#deletionForm').serialize(),
            url: "/deleteEmployee",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                if (data.message == 'success') {
                    $('#deletionForm').trigger("reset");
                    $('#deleteModal').modal('hide');
                    swal({
                        title: 'Success',
                        text: 'Employee deleted successfully.',
                        type: 'success'
                    });
                    ytable.draw();
                } else {
                    $('#deletionForm').trigger("reset");
                    $('#deleteModal').modal('hide');
                    swal({
                        title: 'Oops',
                        text: 'Unexpected error occurred while trying to process your request.',
                        type: 'error'
                    });
                    ytable.draw();
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
});

/** check email exist */
function checkEmail()
{
    var userMail = $('#email').val();
    var token_val = $( "input[name='_token']" ).val();
    $.ajax({
        url: "/checkUserMail",
        type: "POST",
        dataType: 'json',
        data: {
            _token: token_val,
            userMail: userMail
        },
        success: function (data) {
            if (data.message == 'mail_exist') {
                $('#mail_exist_err').show();
                $('#email').val('');
            } else {
                $('#mail_exist_err').hide();
            }
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}
</script>
@stop
