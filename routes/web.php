<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/deleteEmployee', [App\Http\Controllers\HomeController::class, 'delete'])->name('deleteEmployee');

Route::post('/checkUserMail', [App\Http\Controllers\HomeController::class, 'checkEmail'])->name('checkUserMail');

Route::post('/saveEmployee', [App\Http\Controllers\HomeController::class, 'save'])->name('saveEmployee');

Route::get('/getEmployee/{id}', [App\Http\Controllers\HomeController::class, 'getEmpDetails'])->name('getEmployee');

Route::post('/updateEmployee', [App\Http\Controllers\HomeController::class, 'update'])->name('updateEmployee');
