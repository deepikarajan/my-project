<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DataTables;
use Exception;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $sort_colum = (isset($request->input('order')[0])) ? $request->input('order')[0]['column'] : '';
            $sort_direction = (isset($request->input('order')[0])) ? $request->input('order')[0]['dir'] : '';
            $data =  User::select('users.id','users.name','users.email','r.name as designation')
                        ->join('roles as r','r.id','=','users.role_id')->where('r.id','<>',1);           
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->filter(function ($query) use ($request, $sort_colum, $sort_direction) {
                        if ($request->input('search')['value'] != '') {
                            $query->where(function ($query) use($request) {
                                $query->where('users.name', 'like', '%' . $request->input('search')['value'] . '%')
                                        ->orWhere('users.email', 'like', '%' . $request->input('search')['value'] . '%')
                                        ->orWhere('designation', 'like', '%' . $request->input('search')['value'] . '%');
                            });
                        }
                        if($sort_colum == ''){
                            $query->orderBy('users.id', 'desc');
                        }else{
                            if($sort_colum == 0){
                                $query->orderBy('users.name', $sort_direction);
                            }else if($sort_colum == 1){
                                $query->orderBy('users.email', $sort_direction);
                            }else if($sort_colum == 2){
                                $query->orderBy('users.designation', $sort_direction);
                            } 
                        }
                    })
                    ->addColumn('name', function($row){  
                        return $row['name'];
                    })
                    ->addColumn('email', function($row){  
                        return $row['email'];
                    })
                    ->addColumn('designation', function($row){  
                        return $row['designation'];
                    })
                    ->addColumn('action', function($row){
                           $btn = '<div class="d-flex justify-content-end">
                                        <button class="btn btn-link hover-primary p-1 m-1" id="editEmp" name="editEmp" data-id="'.$row['id'].'"><a href="javascript:void(0)" style="color:blue;"><i class="fas fa-pencil-alt"></i></a></button>
                                        <button class="btn btn-link hover-primary p-1 m-1" id="deleteEmp" data-id="'.$row['id'].'" name="deleteElection"><i class="fas fa-trash-alt"></i></button>
                                    </div>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        $roles = DB::table('roles')->where('id','<>',1)->get();
        return view('home',compact('roles'));
    }

    public function save(Request $request) {
        try {
            Validator::make($request->all(), [
                'email' => ['required', 'email', 'string'],
                'name' => ['required', 'string', 'max:255'],
                'designation' => 'required',
            ])->validate();

            if ($request->hasFile('profile_pic')) {
                $filename = $request->profile_pic->getClientOriginalName();
                $request->profile_pic->storeAs('images', $filename, 'public');
                $profile = $filename;
            } else {
                $profile = '';
            }

            $user = User::create([
                'name' => $request['name'],
                'profile_pic' => $profile,
                'email' => $request['email'],
                'password' => Hash::make(Str::random(10)),
                'role_id' => $request['designation']
            ]);

            event(new Registered($user));

            return redirect()->route('home')
                ->with('success_message', 'Employee was successfully added.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    public function getEmpDetails($id) {
        try {
            $user = User::where('users.id',$id)
                    ->join('roles as r','r.id','=','users.role_id')
                    ->select('users.id as user_id','users.name','users.role_id','r.name as role_name','users.email')
                    ->first();

            return response()->json([
                'message' => 'success',
                'result' => $user,
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'message' => 'error',
            ]);
        }
    }

    public function update(Request $request) {
        try {
            Validator::make($request->all(), [
                'edit_email' => ['required', 'email', 'string'],
                'edit_name' => ['required', 'string', 'max:255'],
                'edit_designation' => 'required',
            ])->validate();

            $userId = $request['user_id'];
            $user = User::findOrFail($userId);
            $user->name = $request['edit_name'];
            $user->role_id = $request['edit_designation'];
            $user->save();

            return redirect()->route('home')
                ->with('success_message', 'Employee was successfully updated.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    public function delete(Request $request) {
        try {
            $empId = $request['emp_id'];
            $employee = User::findOrFail($empId);
            $employee->delete();

            return response()->json([
                'message' => 'success',
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'message' => 'error',
            ]);
        }
    }

    public function checkEmail(Request $request) {
        $userMail = $request->userMail;
        $userMailExist = User::where('email',$userMail)->first();
        if($userMailExist) {
            return response()->json([
                'message' => 'mail_exist',
            ]);
        } else {
            return response()->json([
                'message' => 'no_mail',
            ]);
        }
    }
}
